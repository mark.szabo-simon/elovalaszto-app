<?php
namespace App\Policy;

use App\Model\Entity\candidate;
use Authorization\IdentityInterface;

/**
 * candidate policy
 */
class candidatePolicy
{
    /**
     * Check if $user can create candidate
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\candidate $candidate
     * @return bool
     */
    public function canAdd(IdentityInterface $user, candidate $candidate)
    {
        if ($user->role === 3) {return true;} else {return false;}
    }

    /**
     * Check if $user can update candidate
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\candidate $candidate
     * @return bool
     */
    public function canEdit(IdentityInterface $user, candidate $candidate)
    {
        if ($user->role === 3) {return true;} else {return false;}
    }
    public function canDelete(IdentityInterface $user, candidate $candidate)
    {
        if ($user->role === 3) {return true;} else {return false;}
    }


    /**
     * Check if $user can view candidate
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\candidate $candidate
     * @return bool
     */
    public function canView(IdentityInterface $user, candidate $candidate)
    {
    }
}
