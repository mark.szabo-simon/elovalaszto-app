<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\Utility\Security;
use Cake\Http\Cookie\Cookie;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Http\Cookie\CookieCollection;
use Cake\Utility\Xml;
use DateTime;
use Cake\Core\Configure;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['lakcimkartya', 'bejelentkezes', 'pdf', 'regisztracio', 'metabase', 'pin', 'kijelentkezes', 'miertNemTudokSzavazni', 'resetPassword']);
    }


    public function resetPassword() {
        $this->Authorization->skipAuthorization();
        $user = $this->Users->newEntity();
        if ($this->request->is('POST')) {
            $user = $this->Users->findByUsername($this->request->getData('username'))->first();
            if (null === $user->email) {
                $this->Flash->error('A felhasználóhoz nem tartozik e-mail fiók!');
                return $this->redirect('/');
            }
            if (null != $user) {
                $user->pin = mt_rand(1000000, 9999999);
                if ($result = $this->Users->saveWithMessage($user, 'Új jelszó megadásához kattints a következő linkre: https://elovalaszto.hu/users/pin/' .  $user->id . '/?pin=' . $user->pin, 'Jelszavad az Előválasztóhoz')) {
                    $this->Flash->success(__('Kiküldésre került az aktivációs link az e-mail címére!'));
                    return $this->redirect('/');
                } else {
                    $this->Flash->error(__('A szavazó hozzáadása nem sikerült, ellenőrizd a megadott adatokat!'));

                }

            }
        }
        $this->set(compact('user'));

    }

    public function addVolounteer() {

        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        $user = $this->Users->newEntity();
        if ($this->request->is('POST')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $docnum = mt_rand(1000, 9999);
            $user->id = Security::hash($docnum, 'sha256', true);
            $user->role = 1;
            $user->place_created = 1;
            $user->lastletters = substr(preg_replace('/\s+/', '', $this->request->getData('docnum')), -2);
            $user->created_by = $this->Authentication->getIdentity()->id;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Az önkéntes sikeresen hozzáadásra került!'));
                $this->redirect(['action' => 'dashboard']);
            }
        }
        $this->set('user', $user);
    }

    public function anonymizeSelf($id) {
        $user = $this->Authentication->getIdentity();
        $this->Authorization->can($user, 'myself');
        if ($this->request->is('POST')) {
            if ($result = $this->Users->anonymize($user->id)) {
                $this->Flash->success(__('Személyes adatait töröltük a rendszerből, ellenben újra regisztrálni nem fog tudni.'));
                $this->Authentication->logout();
                $this->request->getSession()->clear();
                $this->redirect('/');
            } else {
                $this->Flash->error(__('Nem sikerült az anonimizálás, az adminisztrátor értesítésre került!'));
                $this->redirect(['action' => 'home']);
            }
        }
    }


    public function createvolounteer () {
        $user = $this->Users->newEntity();
        $this->Authorization->authorize($user, 'createvolounteer'); //TODO: Authorization Policy!
        if ($this->request->is('post')) {
            $this->Users->patchEntity($user, $this->request->getData());
            $user->role = 2;
            if($result = $this->Users->save($user)) {
                $this->Flash->success(__('Sikeresen létrehoztad az önkéntest!'));
            } else {
                $this->Flash->error(__('A felhasználó létrehozása nem sikerült!'));
            }
        }
    }

    public function bejelentkezes () {

        $user = $this->Users->newEntity();
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $redirect = $this->request->getQuery('redirect', ['action' => 'dashboard']);
            return $this->redirect($redirect);
        }

        if ($this->request->is(['post']) && !$result->isValid()) {
            $this->Flash->error(__('Hibás felhasználónév vagy jelszó!'));
        }

        $this->set(compact('user'));
    }

    public function kijelentkezes () {
        $this->Authorization->skipAuthorization();
        $this->Flash->success(__('Sikeres kijelentkezés!'));
        $this->Authentication->logout();
        $this->request->getSession()->clear();
        $this->redirect('/');
        
    }

    public function setPlace() {
        $this->loadModel('Places');
        $place = $this->Places->newEntity();
        $user = $this->Authentication->getIdentity();
        $this->Authorization->authorize($user, 'volounteer');
        $places = $this->Places->find('list')->where(['id !=' =>  '1'])->order(['name' => 'ASC']); // 
        if ($this->request->is('POST')) {
            $this->request->getSession()->write('place_id', $this->request->getData('place_id'));
            $this->Flash->success(__('Sikeresen beállítottad a helyet.'));
            return $this->redirect(['action' => 'dashboard']);
        }
        $this->set(compact('places', 'place'));
    }


    public function offlineRegister () {
        if (true) {
            $this->Flash->success(__('Már nem lehetséges!'));            
            return $this->redirect(['action' => 'dashboard']);
        }

        if (!$this->request->getSession()->check('place_id')) { // csak sátorba bejelentkezve lehessen regisztrálni
            $this->redirect(['action' => 'setPlace']);
        }
        $user = $this->Users->newEntity();
        $this->Authorization->authorize($user, 'volounteer');
        if ($this->request->is('POST')) {
            $docnum = Security::hash(strtoupper($this->request->getData('docnum')), 'sha256', true);

            if (is_array($this->Users->canRegister($docnum))) {
                $this->Flash->error($this->Users->canRegister($docnum)[0]);
            } else {
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user->id = $docnum;
                $user->lastletters = substr(preg_replace('/\s+/', '', $this->request->getData('docnum')), -2);
                $user->role = 0;
                $user->valid = 1;
                $user->verified = 1;
                $user->place_created = $this->request->getSession()->read('place_id');
                $user->pin = mt_rand(1000000, 9999999);
                $user->password = Text::uuid();
                $user->created_by = $this->Authentication->getIdentity()->id;
                if ($result = $this->Users->saveWithMessage($user, 'Új jelszó megadásához kattints a következő linkre: https://elovalaszto.hu/users/pin/' .  $user->id . '/?pin=' . $user->pin, 'Jelszavad az Előválasztóhoz')) {
                    $this->Flash->success(__('A szavazó számára kiküldésre került az aktiváló link!'));
                    $this->Flash->success(__('A szavazó későbbi online szavazásra sikeresen hozzáadásra került!'));
                    $this->redirect(['action' => 'offlineRegister']);
                } else {
                    $this->Flash->error(__('A szavazó hozzáadása nem sikerült, ellenőrizd a megadott adatokat!'));

                }

            }
        }
        $this->set('user', $user);

    }


    public function offlineVote () {
        $user = $this->Users->newEntity();
        $this->Authorization->authorize($user, 'volounteer');
        // HARDCODE place_id based on username... ;=/
        switch ($this->Authentication->getIdentity()->username) {
            case 'nyugatitér':
                $sator = 2;
                break;
            case 'blahalujza':
                $sator = 3;
                break;
            case 'széllkálmán':
                $sator = 4;
                break;
            case 'móricz':
                $sator = 5;
                break;
            case 'lehel':
                $sator = 6;
                break;
            case 'boráros':
                $sator = 7;
                break;
            case 'nagyvárad':
                $sator = 8;
                break;
            case 'újpest':
                $sator = 9;
                break;
            case 'flórián':
                $sator = 10;
                break;
            case 'határút':
                $sator = 11;
                break;
            case 'csepel':
                $sator = 12;
                break;
            case 'újpalota':
                $sator = 13;
                break;
            case 'rákos':
                $sator = 14;
                break;
            case 'köki':
                $sator = 16;
                break;
            case 'örsvezér':
                $sator = 17;
                break;
            
            default:
                $this->Flash->error('Csak sátorhoz beosztott önkéntesek tudnak szavazni.');
                return $this->redirect(['action' => 'dashboard']);
                break;
        }

        if (!$this->request->getSession()->check('place_id')) { // csak sátorba bejelentkezve lehessen regisztrálni
            $this->request->getSession()->write('place_id', $sator);
        }
        if ($this->request->is('POST')) {
            $docnum = Security::hash(strtoupper($this->request->getData('docnum')), 'sha256', true);
            // Ha már regisztrált online, de offline megy el szavazni, akkor állítsuk szavazottra
            if (0 != $this->Users->find()->where(['id' => $docnum])->count()) {
                $user = $this->Users->get($docnum);
                $reasons = $this->Users->canVote($user->id);
                if (!is_array($reasons)) {
                    $user->voted = true;
                    $user->valid = true;
                    $user->verified = true;
                    $user->vote_time = new DateTime('now');
                    $user->place_voted = $this->request->getSession()->read('place_id');
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('Az jelenlévő ügyféltörzset szavazottra állítottam!'));
                    } else {
                        $this->Flash->error(__('Hiba a mentés során!'));
                    }
                } else {
                    $reasontext = '';
                    foreach ($reasons as $reason) {
                        $reasontext .= $reason . '<br>';
                    }
                    $this->Flash->error($reasontext, ['escape' => false]);
                    $this->redirect($this->referer());
                }
            } else { // most látjuk először a lakcímkártyáját
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user->id = $docnum;
                $user->verified = true;
                $user->valid = true;
                $user->pin_ok = true;
                $user->place_created = $this->request->getSession()->read('place_id');
                $user->place_voted = $this->request->getSession()->read('place_id');

                $user->username = Text::uuid();
                $user->password = Text::uuid();
                $user->voted = true;
                $user->vote_time = new DateTime('now');

                $user->lastletters = substr(preg_replace('/\s+/', '', strtoupper($this->request->getData('docnum'))), -2);
                $user->role = 0;
                $user->created_by = $this->Authentication->getIdentity()->id;
                if ($result = $this->Users->save($user)) {
                    $this->Flash->success(__('<h3>OK, kérlek add oda a szavazónak a szavazólapot!<h3>'), ['escape' => false]);
                    $this->redirect(['action' => 'offlineVote']);
                } else {
                    $this->Flash->error(__('A szavazó hozzáadása nem sikerült, ellenőrizd a megadott adatokat!'));

                }

            }
        }
        $this->set('user', $user);        
    }


    public function pdf() {
        if (new DateTime('2019-06-20 08:00:00') < new DateTime('now')) {
            $this->set('regperiod_finished', true);
        }
        $this->Authorization->skipAuthorization();
        if ($this->Authentication->getIdentity()) {
            $this->Flash->success(__('Már be vagy jelentkezve.'));
            return $this->redirect(['action' => 'dashboard']);
        }
        // if ($this->request->getSession()->check('Auth')) {dd($this->Authentication->getIdentity());}
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            if ($this->request->getData('hungarian') == 0) {
                $this->Flash->error(__('Csak az Európai Unió állampolgárai vehetnek részt a szavazáson'));
                return $this->redirect(['action' => 'pdf']);
            }
            if ($this->request->getData('acceptance') == 0) {
                $this->Flash->error(__('El kell fogadni a felhasználási feltételeket a továbblépéshez!'));
                return $this->redirect(['action' => 'pdf']);
            }
            $uuid = Text::uuid();
            $filename = 'avdh.pdf';
            $filePath = ROOT . DS . 'pdfs' . DS . $uuid . DS . $filename;
            
            $file = new File($this->request->getData('submittedfile')['tmp_name']);
            if($file->mime() != 'application/pdf') {
                $this->Flash->error(__('Csak PDF tipusú file tölthető fel!'));
                return $this->redirect(['action' => 'pdf']);
            }
            mkdir(ROOT . DS . 'pdfs' . DS . $uuid);

            if(move_uploaded_file(
                    $this->request->getData('submittedfile')['tmp_name'],
                    $filePath
            )) {
            $result = $this->__validateSignature($filePath, $uuid);
                    if ($result['result'] != 'error') { 
                        $this->Flash->success($result['message']);
                        $this->redirect(['action' => 'regisztracio']);
                    } else {
                        $this->Flash->error(__('A pdf file ellenőrzése során a következő hiba lépett fel:') . '<br>' . $result['message'], ['escape' => false]);
                    }
                } else {
                    
                    $this->Flash->error(__('A pdf file feltöltése nem sikerült!'));
                }
            $trashdir = new Folder(ROOT . DS . 'pdfs' . DS . $uuid );
            $trashdir->delete();    
        }
        $this->set(compact('user', 'adatok'));        
    }

    public function miertNemTudokSzavazni() {
        $this->Authorization->skipAuthorization();
        $reasons = [];
        if (!$this->Authentication->getIdentity()) {
            array_push($reasons, __('Mert nem vagy bejelentkezve! Megoldás: <a href="/users/bejelentkezes">Jelentkezz be!</a>'));
        } else {
            $reasons = $this->Users->canVote($this->Authentication->getIdentity()->id);
        }
        $user = $this->Users->find()->where(['id' => $this->Authentication->getIdentity()]);
        $this->set('reasons', $reasons);
    }


    public function metabase() {
        $this->Authorization->skipAuthorization();
        // $metabaseUrl = 'http://localhost:3000';
        // $metabaseKey = '3a9fcb6c25b2dcd2db880cd444d8aacb1933692b3b551bfa85fa0cb4638a4a62';
        // $metabase = new \Metabase\Embed($metabaseUrl, $metabaseKey);
        // $params = ['date' => 'past26weeks'];
        // $dashboardId = 1;
        // $this->set('iframe', $metabase->dashboardIframe($dashboardId, $params));
    }


    public function regisztracio() {
        $this->Authorization->skipAuthorization();
        $session = $this->getRequest()->getSession();
        if (new DateTime('2019-06-20 08:00:00') < new DateTime('now')) {
            $this->Flash->error(__('A regisztrációs időszak lezárult, személyesen lehet részt venni a szavazáson!'));
            return $this->redirect(['action' => 'pdf']);
        }

        if (!$session->check('pdfadatok')) {
            $this->Flash->error(__('A regisztrációs folyamat Ügyfélkapus pdf segítségével indítható el!'));
            $this->redirect('/');
        } else {
            $user = $this->Users->newEntity();
            if ($this->request->is('POST')) {
                $user = $this->Users->newEntity($this->request->getData());
                $user->id = $session->read('pdfadatok.hash.value');
                $user->signer_email_hash = $session->read('pdfadatok.signer_email_hash.value');
                $user->lastletters = $session->read('pdfadatok.lastletters.value');
                $user->created_by = 'AVDH';
                $user->place_created = 1;
                $user->verified = true;
                $user->mobile = $this->request->getData('mobile');
                $user->docnum = $this->request->getSession()->read('docnum');
                $user->password = Text::uuid();
                $user->valid = true;
                $user->birthdate = $session->read('pdfadatok.birthdate.value');
                $user->acceptance = true;
                $user->zip = $session->read('pdfadatok.zip.value');
                $user->pin = mt_rand(1000000, 9999999);
                if ($result = $this->Users->saveWithMessage($user, 'Új jelszó megadásához kattints a következő linkre: https:/elovalaszto.hu/users/pin/' .  $user->id . '/?pin=' . $user->pin, 'Aktivációs link az Előválasztóhoz')) {
                    $this->Flash->success(__('A megadott e-mail címre kiküldtünk egy egyszer használatos aktivációs linket. Ezzel véglegesíthető a regisztráció. ') . '<br><strong style="color:#cd2a3e;">' .  __('Emlékeztetőül, a választott felhasználónév: ') . $result->username . '</strong>', ['escape' => false]);
                    return $this->redirect('/');
                } else {
                    $this->Flash->error(__('A szavazó hozzáadása nem sikerült, ellenőrizd a megadott adatokat!'));

                }

            }
            $this->set(compact('user'));

        }
    }

    public function dashboard () {
        $this->Authorization->skipAuthorization();
        $options = [ // minden szerepkörhöz más lehetőségek társulnak
            '0' => [
                ['label' => 'Szavazás', 'url' => ['controller' => 'votes', 'action' => 'add']],
                ['label' => 'Személyes adatok', 'url' => ['action' => 'adatok']],
                ['label' => 'Kijelentkezés', 'url' => ['action' => 'kijelentkezes']],
            ],
            '1' => [
                // ['label' => 'Online regisztráció ellenőrzése', 'url' => ['action' => 'offlineRegister']],
                // ['label' => 'Offline regisztrálás online szavazásra (sátras)', 'url' => ['action' => 'offlineRegister']],
                ['label' => 'Offline szavazás', 'url' => ['action' => 'offlineVote']], 
                ['label' => 'Kijelentkezés', 'url' => ['action' => 'kijelentkezes']],

            ],
            '3' => [
                ['label' => 'Jelöltek hozzáadása', 'url' =>['controller' => 'candidates', 'action' => 'add']],
                ['label' => 'Önkéntesek hozzáadása', 'url' =>['controller' => 'users', 'action' => 'addVolounteer']],
                ['label' => 'Önkéntesek menedzselése', 'url' =>['controller' => 'users', 'action' => 'volounteerlist']],
                ['label' => 'Szavazóhelyek hozzáadása', 'url' =>['controller' => 'places', 'action' => 'add']],
                ['label' => 'Adatellenőrzés', 'url' =>['action' => 'adatellenorzes']],
                ['label' => 'Kijelentkezés', 'url' => ['action' => 'kijelentkezes']],

            ]
        ];
        $this->set('options', $options[$this->Authentication->getIdentity()->role]);
    }

    public function adatok() {
        $user = $this->Users->get($this->Authentication->getIdentity()->id);
        
        if ($user->voted) {
            $this->loadModel('Places');
            $place = $this->Places->get($user->place_voted);
            $this->set('place', $place);
        }
        if (!$this->Authorization->can($user, 'Fiok')) {
            $this->Flash->error(__('Csak a saját fiókadataidat tudod megjeleníteni'));
            return $this->redirect('/');
        }
        $this->set('user', $user);

    }

    public function volounteerlist() {
        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        $users = $this->Users->find()->where(['role' => '1']);
        $this->set('users', $users);
    }

    public function pin($id = null) {

        $this->Authorization->skipAuthorization();
        $user = $this->Users->get($id);
        $user->password = null;
        if (($this->request->getQuery('pin') == $user->pin) && ($this->request->getQuery('pin') != null)) {
            if ($this->request->is(['POST', 'PUT'])) {
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user->pin_ok = true;
                $user->pin = null;
                if ($result = $this->Users->save($user)) {
                    $this->Flash->success(__('Sikeresen elmentette a jelszavát. Lépjen be a fiókjába felhasználónevével és jelszavával, hogy tudjon szavazni.'));
                    return $this->redirect(['action' => 'bejelentkezes']);
                } else {
                    $this->Flash->error(__('Nem sikerült elmenteni a jelszót!'));
                }

            } else {
                $this->Flash->success(__('Az ellenőrzés sikeresen megtörtént, a kérjük, adja meg a későbbiekben használni kívánt jelszavát!'));
                $user->password = null;
            }
        } else {
            $this->Flash->error(__('Helytelen aktivácós link!'));
        }
        $this->set('user', $user);
    }

    public function signerhashunlock ($id = null) {
        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        if ($this->request->is('POST')) {
            $user = $this->Users->get($id);
            $user->signer_email_hash = Text::uuid() . 'signerhashunlocked';
            if ($this->Users->save($user)) {
                $this->Flash->success('Aláíró hash felszabadítva');
                return $this->redirect($this->referer);
            }
            
        }

    }

    public function adatellenorzes() {
        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        // $user = $this->Users->newEntity();
        $user = null;
        if ($this->request->is('POST')) {
            $conditions = [];
            if($this->request->getData('username') != "") {array_push($conditions, [ 'username' => $this->request->getData('username')]);}
            if($this->request->getData('signer_email_hash') != "") {array_push($conditions, [ 'signer_email_hash' => Security::hash($this->request->getData('signer_email_hash'), 'sha256', true)]);}
            if($this->request->getData('docnum') != "") {array_push($conditions, ['id' => Security::hash($this->request->getData('docnum'), 'sha256', true)]);}
            if($this->request->getData('email') != "") {array_push($conditions, ['email' => $this->request->getData('email')]);}
            
            $user = $this->Users->find()->where($conditions);
        }
        $this->set('user', $user);
    }

    public function resendpins () {
        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        if ($this->request->is('POST')) {
            $emails = 0;
            $errors = 0;
            $users = $this->Users->find()->where(['pin_ok' => null]);
            foreach ($users as $user) {
                if (
                     $this->Users->__notifyUser($user, 'Kedves Előválasztó! Korábban Önnek kiküldtük személyes aktiváló linkjét, amit jelen pillanatig még nem használt. Azért küldjük ezt az üzenetet, mert mi is fontosnak érezzük az előválasztáson való részvételt. Kérjük, használja a következő linket a jelszó megadásához. Felhasználónevét megadta a regisztráció során. Ha mégis akadályba ütközne, kérjük jelezze nekünk az elovalaszto@ahang.hu címen. Új jelszó megadásához kattintson a következő linkre: https://elovalaszto.hu/users/pin/' .  $user->id . '/?pin=' . $user->pin, 'Még nem aktiválta az Előválasztó fiókját!')
                ) {
                    $emails ++;
                } else {
                    $errors ++;
                }  
            }
            $this->Flash->success(__('Sikeresen kiküldésre került {0} felhasználónak, {1} db hiba történt', [$emails, $errors]));
            $this->redirect($this->referer());
        }

    }

    private function __validateSignature($filePath = null, $uuid = null) {
        $origDir = new Folder(ROOT . DS . 'pdfs' . DS . $uuid);
        $validationErrors = [];
        $signatureArray = explode(PHP_EOL, shell_exec('pdfsig ' . escapeshellarg($filePath)));
        // ALÁÍRÁS CSEKKEK
        if ($signatureArray[0] == 'File \'' . $filePath . '\' does not contain any signatures') return ['result' => 'error', 'message' => __('A dokumentum nincs aláírva')];
        if ($signatureArray[9] != '  - Signature Validation: Signature is Valid.') return ['result' => 'error', 'message' => __('A dokumentum aláírását nem lehetett ellenőrizni')];
        if ($signatureArray[2] != '  - Signer Certificate Common Name: AVDH Bélyegző') return ['result' => 'error', 'message' => __('A dokumentumot nem az AVDH Bélyegzővel írták alá')];
        if ($signatureArray[10] != '  - Certificate Validation: Certificate is Trusted.') return ['result' => 'error', 'message' => __('A dokumentum aláírásának hitelességét nem lehetett ellenőrizni.')];
        // további PDF validációk (külön trackelve)
        if (false) return ['result' => 'error', 'message' => __('A dokumentum eredetiségével kapcsolatban problémák merültek fel. Kérjük, lépjen mihamarabb kapcsolatba velünk elérhetőségeink bármelyikén.')];
        // További validációk vége
        $igazolasPWD = ROOT . DS . 'pdfs' . DS . $uuid . DS . 'igazolas';
        $dir = new Folder($igazolasPWD, true);
        shell_exec('pdfdetach '. escapeshellarg($filePath) . ' -o ' . escapeshellarg($igazolasPWD) . ' -saveall ');
        $files = $dir->find('.*\.pdf', true);
        shell_exec('pdfdetach ' . escapeshellarg($igazolasPWD . DS . $files[0]) . ' -o ' . escapeshellarg($igazolasPWD) . ' -saveall ');
        $xmlArray = Xml::toArray(Xml::build($igazolasPWD . DS . 'meghatalmazo.xml'));
        $email = $xmlArray['ArtifactResponse']['saml2p:Response']['saml2p:Assertion']['saml2p:Subject']['saml2p:NameID']['@'];
        if (false === strpos($email, '@')) {
            return ['result' => 'error', 'message' => __('HIBA: Az aláíráshoz nem tartozik ügyfélkapus e-mail cím!')];
        } else { $emailhash = Security::hash($email, 'sha256', true); }
        $parser = new \Smalot\PdfParser\Parser();
        $pdf    = $parser->parseFile($filePath);
        $details  = $pdf->getDetails();
        $pages = $pdf->getPages();
        $text = $pdf->getText();

        if ($this->Users->find()->where(['signer_email_hash' => $emailhash])->count() != 0) {
            return ['result' => 'error', 'message' => 'Egy ügyfélkapus azonosítóhoz csak egy ügyfélkapus belépés tartozhat.'];
        }

//We need to have no additional chars before line break!
$delimiter = [
    'birthdate' => ['start' => 'Születési dátum', 'end' => 'Neme'],
    'docnum' => ['start' => 'Személyi azonosítót és lakcímet igazoló hatósági igazolvány adatok
Okmányazonosító', 'end' => 'Érvényesség'],
    'address' => ['start' => 'Lakóhely adatok
Cím', 'end' => 'Bejelentés dátuma']];
                        $adatok = [];
                        foreach ($delimiter as $kulcs => $ertek) {
                                $adatok[$kulcs] = $this->__get_string_between($text, $ertek['start'], $ertek['end']);
                                // array_push($adatok[$kulcs], [$kulcs => ]);
                        }
                        $adatok['zip'] = explode(' ', $adatok['address'])[0];
                        if (!in_array($adatok['zip'], [1069,1011,1012,1013,1014,1015,1016,1021,1022,1023,1024,1025,1026,1027,1028,1029,1031,1032,1033,1034,1035,1036,1037,1038,1039,1041,1042,1043,1044,1045,1046,1047,1048,1051,1052,1053,1054,1055,1056,1061,1062,1063,1064,1065,1066,1067,1068,1071,1072,1073,1074,1075,1076,1077,1078,1081,1082,1083,1084,1085,1086,1087,1088,1089,1091,1092,1093,1094,1095,1096,1097,1098,1101,1102,1103,1104,1105,1106,1107,1108,1111,1112,1113,1114,1115,1116,1117,1118,1119,1121,1122,1123,1124,1125,1126,1131,1132,1133,1134,1135,1136,1137,1138,1139,1141,1142,1143,1144,1145,1146,1147,1148,1149,1151,1152,1153,1154,1155,1156,1157,1158,1161,1162,1163,1164,1165,1171,1172,1173,1174,1181,1182,1183,1184,1185,1186,1188,1191,1192,1193,1194,1195,1196,1201,1202,1203,1204,1205,1211,1212,1213,1214,1215,1221,1222,1223,1224,1225,1237,1238,1239,1529])) {
                            return ['result' => 'error', 'message' => __('Sajnáljuk, az online előszavazásra csak állandó budapesti lakhellyel rendelkezők tudnak regisztrálni. Amennyiben 30 napnál régebbi ideiglenes lakcímmel rendelkezik, vegyen részt az előválasztáson személyesen!')];
                        }
                        if ($adatok['docnum'] == '') {
                            return ['result' => 'error', 'message' => __('A feltöltött pdf-ben nincsenek lakcímkártya adatok. Kérem ellenőrizze. Ha nincs lakcímkártyája, akkor személyesen regisztráljon, és utána szavazhat online, vagy menjen el szavazni a szavazási időszakban. Megértését köszönjük.')];
                        }

                        $pdfadatok = [
                            'hash' => [
                                'description' => __('Lakcímkártyaszám hash'),
                                'value' => Security::hash($adatok['docnum'], 'sha256', true)
                            ],
                            'lastletters' => [
                                'description' => __('Lakcímkártyaszám utolsó két jegye'),
                                'value' => substr(preg_replace('/\s+/', '', $adatok['docnum']), -2)
                            ],
                            'birthdate' => [
                                'description' => __('Születési dátum'),
                                'value' => $adatok['birthdate']
                            ],
                            'zip' => [
                                'description' => __('Irányítószám'),
                                'value' => $adatok['zip']
                            ],
                            'signer_email_hash' => [
                                'description' => __('AVDH aláírás azonosító'),
                                'value' => $emailhash
                            ],
                        ];
                        
                        // Itt semmisítjük meg az adatokat


                if (
                    !$this->Users->find()->where(['id' => $pdfadatok['hash']['value']])->isEmpty()) {
                    $this->request->getSession()->delete('pdfadatok');
                    return ['result' => 'error', 'message' =>  __('A megadott adatokkal már létezik felhasználói fiók. Ez bizonyosan pont az Öné, belépéshez kérem használja az elfelejtett jelszó funkciót a bejelentkezés képernyő alatt!') ];
                } else {
                    $this->getRequest()->getSession()->write('pdfadatok', $pdfadatok );
                    $this->getRequest()->getSession()->write('docnum', $adatok['docnum'] );
                    return ['result' => 'success', 'message' => __('Az adatok kinyerése és a PDF hitelesítése sikeresen lezajlott. A PDF file-t sikeresen megsemmisítettük.')];
                }




    



    }

    private function __get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
    }


}
