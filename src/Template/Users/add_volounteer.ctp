        <section class="section">
          <div class="section-header">
            <h1>Önkéntes regisztrálása</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
                <?php
                echo $this->Form->create($user);
                echo $this->Form->control('username', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'Felhasználónév:']);
                echo $this->Form->control('password', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'Jelszó:']);
                echo $this->Form->control('zip', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'Irányítószám:']);
                echo $this->Form->control('docnum', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'Lakcímkártya száma:'] );
                echo $this->Form->control('mobile', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'Mobiltelefonszám:']);
                echo $this->Form->control('email', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'E-mail cím:']);
                echo $this->Form->control('birthdate', ['type'=> 'date', 'label' => 'Születési dátum:', 'minYear' => '1920'], ['class' => 'form-control']);
                
                $this->Form->setTemplates([
                'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="label-inline">{{text}}</label>',
                'formGroup' => '{{input}}{{label}}',
                ]);

                echo $this->Form->control('acceptance', ['label' => 'Felhasználási feltételeket és titoktartási nyilatkozatot elfogadta']); ?>
                <div style="text-align: center; margin-top: 20px;"><?php echo $this->Form->button(__('Önkéntes regisztrálása'), ['class' => 'btn btn-outline-primary']); ?></div>
                <?php echo $this->Form->end();
                ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>
<script>
  $(document).ready(function(){
    $("body").addClass('addVolounteer');
    $("body.elovalaszto.addVolounteer .input select").addClass('form-control');
});
</script>