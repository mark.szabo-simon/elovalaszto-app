<?php 

                $this->Form->setTemplates([
                'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="label-inline">{{text}}</label>',
                'formGroup' => '{{input}}{{label}}',
                ]);
 ?>

        <section class="section">
          <div class="section-header">
            <h1>Szavazó regisztrálása későbbi online szavazásra</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
              	<?php 
              	echo $this->Form->create($user);
              	echo $this->Form->control('username', ['label' => 'Felhasználónév (jegyezze le papírra magának)', 'class' => 'form-control']);
              	echo $this->Form->control('zip', ['label' => 'Irányítószám', 'class' => 'form-control']);
              	echo $this->Form->control('docnum', ['label' => 'Lakcímkártya száma', 'required' => 'required', 'class' => 'form-control'] );
              	// echo $this->Form->control('mobile', ['label' => 'Mobiltelefonszám', 'required' => 'required'] ) . '<p>*<small>Aktivistáink felhívhatják a megadott számot, hogy ellenőrizzék az adatok helyességét.	</small></p>';
              	echo $this->Form->control('email', ['label' => 'E-mail cím, erre fog menni a megerősítő link', 'required' => 'required', 'class' => 'form-control']);
              	echo $this->Form->control('birthdate', ['type'=> 'date', 'label' => 'Születési dátum', 'minYear' => '1920', 'maxYear' => '2001', 'default' => new DateTime('2001-09-30'), 'class' => 'form-control']);
              	echo $this->Form->control('acceptance', ['label' => 'Felhasználási feltételeket és titoktartási nyilatkozatot elfogadta.','class' => 'form-control']);
                echo $this->Form->control('email_marketable', ['type'=>'checkbox', 'label' => __('Szeretne értesítéseket kapni az előválasztás fejleményeiről és az aHang kezdeményezéseiről emailben (ez nagyon fontos, mert csak így tudunk vele kapcsolatban maradni).')]);?>
                <div style="text-align: center; margin-top: 20px;"><?php echo $this->Form->button(__('Szavazó regisztrálása'), ['class' => 'btn btn-outline-primary']); ?></div>
              	<?php echo $this->Form->end();
              	?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>
        <script>
  $(document).ready(function(){
    $("body").addClass('addVolounteer');
    $("body.elovalaszto.addVolounteer .input select").addClass('form-control');
});
</script>