        <section class="section">
          <div class="section-header">
            <h1>Felhasználói lehetőségek</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">
              Leírás.
            </p> -->
            <div class="row">
            	<?php
            	foreach ($options as $option) {
            	echo '<div class="col-12 col-sm-6 col-lg-6"><div class="card card-light"><div class="card-header"><h4 style="margin: 0 auto;">' . $this->Html->link($option['label'], $option['url'], ['class' => 'btn btn-outline-primary note-btn']) . '</h4></div></div></div>';
            }
                          if ($this->Identity->get('role') === 3) {
                echo '<div class="col-12 col-sm-6 col-lg-6"><div class="card card-light"><div class="card-header"><h4 style="margin: 0 auto;">' . $this->Form->postLink(__('Pin reminder'), ['action' => 'resendpins'], ['class' => 'btn btn-outline-primary note-btn']) . '</h4></div></div></div>';
              }

            ?>
            </div>
          </div>
        </section>