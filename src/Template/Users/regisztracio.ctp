
<?php
$templates = [
	'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="label-inline">{{text}}</label>',
	'formGroup' => '{{input}}{{label}}' ];
?>

        <section class="section">
          <div class="section-header">
            <h1>Regisztráció</h1>
          </div>
          <div class="section-body">

            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->

            <div class="card">
              <div class="card-body" style="margin: 0 auto;">
		<span class="btn btn-icon icon-right btn-success no-event margin-right-15 under-500 under-500-marginbottom-15">1. lépés <i class="fas fa-check-circle"></i></span>
		<span class="btn btn-icon icon-right btn-primary no-event margin-right-15 under-500 under-500-marginbottom-15" <?php echo ($this->getRequest()->getSession()->check('pdfadatok')) ? '' : 'button-outline'?> >2. lépés <i class="fas fa-edit"></i></span>
		<span class="btn btn-outline-primary no-event under-500">3. lépés</span>
              </div>
            </div>

            <?php if ($this->getRequest()->getSession()->check('pdfadatok')): ?>

            <div class="card">
              <div class="card-header">
                <h4>Adatok összegzése</h4>
              </div>
              <div class="card-body">
              	<p><?php echo __('A feltöltött pdf-ből a következő adatokat fogjuk a regisztráció végén adatbázisba menteni:') ?></p>
              	<?php 
              	foreach ($this->getRequest()->getSession()->read('pdfadatok') as $adat) {
              		echo '<p><strong>' . $adat['description'] . '</strong>: <code>' . $adat['value'] .  '</code></p>';
              	}
              	?>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h4><?php echo __('Regisztráció véglegesítése') ?></h4>
              </div>
              <div class="card-body">
              	<?php
              	echo $this->Form->create($user);
              	echo $this->Form->control('username', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => __('Kívánt felhasználónév ( FONTOS! Mindenképp jegyezze fel magának!)'), 'required' => 'required']);
              	echo $this->Form->control('mobile', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => __('Mobilszám <sup>1</sup> (formátum: +36201234567):'), 'escape'=>false, 'required' => 'required']); ?>
              	<?php echo $this->Form->control('email', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'E-mail <sup>2</sup>:', 'escape'=>false, 'required' => 'required']); ?>
              	<?php
              	echo $this->Form->control('mobile_marketable', ['label' => __('Szeretnék értesítéseket kapni az előválasztás fejleményeiről és az aHang kezdeményezéseiről telefonon'), 'templates' => $templates]);
              	echo $this->Form->control('email_marketable', ['type'=>'checkbox', 'label' => __('Szeretnék értesítéseket kapni az előválasztás fejleményeiről és az aHang kezdeményezéseiről emailben (ez nagyon fontos, mert csak így tudunk veled kapcsolatban maradni).'), 'templates' => $templates]);
              	echo $this->Form->button('Tovább', ['class' => 'btn btn-primary', 'style' => 'margin-top: 10px;']);
              	echo $this->Form->end();
              	?>
              </div>
              <div class="card-footer bg-whitesmoke">
                <sup>1</sup> <?php echo __('Aktivistáink felhívhatják a megadott számot, hogy ellenőrizzék az adatok helyességét.') ?><br />
                <sup>2</sup> <?php echo __('Erre az e-mail címre fogjuk az aktiváló kódot elküldeni. A kód nélkül nem fogsz tudni belépni, ezért mindenképp olyan e-mail címet adj meg, amihez hozzáférsz!') ?>
              </div>
            </div>

            <?php endif ?>

          </div>
        </section>