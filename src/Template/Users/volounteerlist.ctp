<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<section class="section">
          <div class="section-header">
            <h1><?= __('Önkéntesek') ?></h1>
          </div>
          <div class="section-body">

        <table class="table table-striped" style="-webkit-border-radius: .25rem; -moz-border-radius: .25rem; border-radius: .25rem; overflow: hidden;">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= h($user->username) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
</section>
