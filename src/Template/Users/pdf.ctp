<?php $this->assign('title', 'Regisztráció PDF segítségével');?>


        <section class="section">
          <div class="section-header">
            <h1>Regisztráció PDF segítségével</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <div class="card-body" style="margin: 0 auto;">
                <span class="btn btn-icon icon-right btn-primary no-event margin-right-15 under-500 under-500-marginbottom-15 <?php echo ($this->getRequest()->getSession()->check('pdfadatok')) ? 'button-outline' : ''?>  button-small">1. lépés <i class="fas fa-edit"></i></span>
                <span class="btn btn-outline-primary no-event margin-right-15 under-500 under-500-marginbottom-15">2. lépés</span>
                <span class="btn btn-outline-primary no-event under-500">3. lépés</span>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <?php
                if (isset($regperiod_finished)) {
                  echo '<h3>Az online regisztrációs időszak lezárult</h3>';
                  echo '<p>Az előszavazáson személyesen lehet részt venni, a Tudnivalók menüpontban felsorolt sátrak egyikében.</p>';
                } else {
                  echo $this->Form->create($user, ['type' => 'file']);
                  echo $this->Form->control('submittedfile', [
                      'type' => 'file', 'label' => 'Pdf file tallózása és feltöltése'
                  ]);
                  $this->Form->setTemplates([
                      'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="label-inline">{{text}}</label>',
                      'formGroup' => '{{input}}{{label}}',
                  ]);
                  echo $this->Form->control('hungarian', ['type' => 'checkbox', 'label' => 'Kijelentem, hogy az Európai Unió állampolgára vagyok, és az általam feltöltött adatok a valóságnak megfelelnek']);
                  echo $this->Form->control('acceptance', ['label' => 'Kifejezetten hozzájárulok személyes adataim kezeléséhez az alábbi tájékoztató (<a href="/pages/adatkezeles" target="_blank">előválasztó GDPR</a> és <a href="https://ahang.hu/adatkezeles" target="blank">Magyarhang GDPR</a>) elolvasása és elfogadása után.', 'escape' => false]);
                  echo $this->Form->button(__('Pdf adatok feltöltése és ellenőrzése'), ['class' => 'btn btn-primary', 'style' => 'text-align: center; margin-top: 10px; width: 100%; display: inline-block;']) ;
                  echo $this->Form->end();                  
                  ?>
                <div class="card" style="margin-top: 20px;">
                  <style>.embed-container {-webkit-border-top-left-radius: .25rem; -webkit-border-top-right-radius: .25rem; -moz-border-radius-topleft: .25rem; -moz-border-radius-topright: .25rem; border-top-left-radius: .25rem; border-top-right-radius: .25rem; margin-top: 10px; position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/ghkuBwSTwhU' frameborder='0' allowfullscreen></iframe></div>
                  <div class="alert alert-light" style="margin: 25px 25px 15px;">A videóban a következő linkek találhatók:</div>
                  <ul style="margin-bottom: 0;">
                    <li><a href="http://elovalaszto.hu/hitelesites/" target="_blank">http://elovalaszto.hu/hitelesites/</a></li>
                    <li><a href="https://www.nyilvantarto.hu/ugyseged/" target="_blank">https://www.nyilvantarto.hu/ugyseged/</a></li>
                    <li><a href="https://niszavdh.gov.hu/" target="_blank">https://niszavdh.gov.hu/</a></li>
                  </ul>
                  <div class="card-body">
                    <div id="accordion">
                      <div class="accordion">
                        <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-1" aria-expanded="false">
                          <h4>Hogyan tölthetem le a rólam nyilvántartott adatokat a nyilvántartóból? (Klikk a lépésről-lépésre útmutatóhoz!)</h4>
                        </div>
                        <div class="accordion-body collapse" id="panel-body-1" data-parent="#accordion" style="">

                          <div class="alert alert-warning" style="margin: 20px 0;"><strong>Az AVDH-s közokirat megszerzésének menete:</strong></div>

                          <div class="alert alert-light" style="margin: 20px 0;">1.Lépj be ‘Ügyfélkapun’ keresztül az alábbi link használatával: <a target="_blank" href="https://www.nyilvantarto.hu/ugyseged/NyilvantartottSzemelyesAdatokLekerdezeseMegjelenitoPage.xhtml">https://www.nyilvantarto.hu/ugyseged/NyilvantartottSzemelyesAdatokLekerdezeseMegjelenitoPage.xhtml</a></div>

                          <?php echo $this->Html->image('1.png', ['class' => 'pdfhow']) ?>

                          <div class="alert alert-light" style="margin: 20px 0;">2. Sikeres belépés után azonnal megjelennek személyes adataid. Görgess le az oldal legaljára s ott kattints a ‘Letöltés’ gombra.</div>

                          <?php echo $this->Html->image('2.png', ['class' => 'pdfhow']) ?>

                          <div class="alert alert-light" style="margin: 20px 0;">3. Az így lementett PDF fájlt töltsd fel az alábbi linken keresztül <a href="https://niszavdh.gov.hu" target="_blank">https://niszavdh.gov.hu</a> ‘Fájl kiválasztása’ – itt töltsd fel a PDF-et – ‘Hiteles PDF’ kipipálása – ‘Elfogadom az ÁSZF-et’ kipipálása – kattintás a ‘Dokumentum elküldése’ gombra:</div>

                          <?php echo $this->Html->image('3.png', ['class' => 'pdfhow']) ?>

                          <div class="alert alert-light" style="margin: 20px 0;">4. Miután rákattintottál a ‘Dokumentum elküldése gombra, az alábbi kép fogad:</div>

                          <?php echo $this->Html->image('4.png', ['class' => 'pdfhow']) ?>

                          <div class="alert alert-light" style="margin: 20px 0;">5. Pipáld ki az ‘Ügyfélkapu’-t és kattints az ‘Azonosítás’ gombra. Eztán a következőt látod:</div>

                          <?php echo $this->Html->image('5.png', ['class' => 'pdfhow']) ?>

                          <div class="alert alert-light" style="margin: 20px 0;">5. Itt újra írd be felhasználóneved és jelszavad majd várj néhány másodpercet, amíg a kért oldal megjelenik</div>

                          <div class="alert alert-light" style="margin: 20px 0;">6. A megjelenő oldalon görgess lefelé s kattints a ‘Dokumentum letöltése’ gombra.</div>

                          <?php echo $this->Html->image('6.png', ['class' => 'pdfhow']) ?>

                          <div class="alert alert-light" style="margin: 20px 0;">7. Ezt az új, ‘avdh’ kezdetű PDF-et töltsd fel oldalunkon.</div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php                 
                  }
                ?>
            </div>
            <div class="card-footer bg-whitesmoke">
                <?php echo $this->Html->link('Vissza', '/', ['class' => 'button']) ?>
            </div>
        </div>

    </div>
</section>

<script>
    document.querySelector('form').addEventListener( 'submit', function (e) {
        if (document.getElementById("submittedfile").files.length == 0)  {
            alert("<?php echo __('Tallózd be az Ügyfélkapuról letöltött pdf-et!') ?>")
            e.preventDefault();            
        }
        if (document.getElementById('hungarian').checked === false) {
            alert("<?php echo __('Csak az Európai Únió állampolgárai vehetnek részt az előválasztáson!') ?>")
            e.preventDefault();
        }
        if (document.getElementById('acceptance').checked === false) {
            alert("<?php echo __('A részvételhez el kell fogadni a felhasználási feltételeket és az adatkezelési tájékoztatót') ?>")
            e.preventDefault();
        }
        
    })
</script>