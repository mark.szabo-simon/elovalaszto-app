<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Candidate $candidate
 */
?>
        <section class="section">
          <div class="section-header">
            <h1><?= __('Edit Candidate') ?></h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body" style="margin: 0 auto;">
                <!-- </p> -->
                <?= $this->Form->postLink(__('Delete'),
                ['action' => 'delete', $candidate->id], ['class' => 'btn btn-secondary margin-right-15 under-500 under-500-marginbottom-15'],
                ['class'=>'button','confirm' => __('Are you sure you want to delete # {0}?', $candidate->id)])?>

                <?= $this->Html->link(__('List Candidates'), ['action' => 'index'], ['class' => 'btn btn-primary margin-right-15 under-500 under-500-marginbottom-15']) ?>
                <?= $this->Html->link(__('List Votes'), ['controller' => 'Votes', 'action' => 'index'], ['class' => 'btn btn-primary margin-right-15 under-500 under-500-marginbottom-15']) ?>
                <?= $this->Html->link(__('New Vote'), ['controller' => 'Votes', 'action' => 'add'], ['class' => 'btn btn-primary under-500']) ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->
                <?= $this->Form->create($candidate) ?>
                <?php
                echo $this->Form->control('name', ['class' => 'form-control marginbottom-15']);
                echo $this->Form->control('party', ['class' => 'form-control']);
                ?>
                <div style="text-align: center; margin-top: 20px;"><?= $this->Form->button(__('Submit'), ['class' => 'btn btn-outline-primary']) ?></div>
                <?= $this->Form->end() ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>

          </div>
        </section>