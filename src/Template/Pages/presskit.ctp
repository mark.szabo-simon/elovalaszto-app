        <section class="section">
          <div class="section-header">
            <h1><?php echo __('Presskit') ?></h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
              	<center><a style="width: 100%;" href="/elovalaszto-presskit.zip" class="btn btn-icon icon-left btn-primary"><i class="fas fa-download"></i> Presskit (zip)</a></center>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>

            <!-- <div class="card">
              <div class="card-header">
                <h4>Sajtóanyag 2019-06-12: Végigvezetünk az úton: indul a regisztráció a budapesti előválasztásra</h4>
              </div>
              <div class="card-body">
              </p>
              	<strong>A budapesti előválasztásra mától június 20-ig lehet regisztrálni – ez az online voksolás előfeltétele. Az aHang lépésről lépésre elmagyarázza a regisztráció módját, sőt „Regisztráló Facebook partit” is tart azoknak, akik elbizonytalanodnak az elsőre talán bonyolultnak tűnő adminisztrációt illetően. Hosszas egyeztetések után megszületett az előválasztás konszenzusos folyamata: a jelöltek június 17-ig mutathatják be az induláshoz szükséges 2000 ajánlást, 19-én (hazánkban) műfajteremtő vitát rendez az aHang, június 20. és 26. között pedig lezajlik Magyarország első, igazi téttel bíró előválasztása, amelynek győztese lesz Tarlós István jelenlegi főpolgármester legfőbb, legesélyesebb kihívója októberben.</strong>
              </p>
              <p>Számtalan egyeztető kör (és mélypont) után, Magyarországon páratlan civil és politikusi összefogás eredményeként valósul meg a főpolgármesteri előválasztás, amely nem csupán lehetőséget teremt arra, hogy egy ellenzéki jelölt valódi esélyesként induljon a kormánypárt delegáltjával szemben, de honosítja az előválasztás gyakorlatát is.</p>
              </div>
            </div> -->

            <div class="card">
              <div class="card-body">
              </p><strong>Regisztráció</strong></p>

              <p>Az aHang csapata hónapok óta azon dolgozik, hogy szintetizálja a lehetséges jelöltek igényeit, illetve megteremtse az online és offline voksolás technikai és jogi feltételeit. Mostanra megkérdőjelezhetetlen rendszert sikerült felállítani az előválasztás tisztaságát garantáló Civil Választási Bizottság (CVB) felügyelete mellett.</p>

              <p>Ma nyílt meg az aHang regisztrációs felülete, amelyen keresztül június 20-án reggel 8 óráig jelentkezhetnek mindazok, akik nem személyesen szeretnék leadni a voksukat. Minderre az Ügyfélkapu segítségével van lehetőség, ez az egyetlen módja annak, hogy ne történjenek visszaélések.</p>

              <p>Infóktól a regisztrációig, a jelöltektől a szavazásig minden egy oldalon: <a href="https://elovalaszto.hu/"><strong>www.elovalszto.hu</strong></a></p>

              <p>A regisztráció lépéseit kisfilmben és szövegesen, screenshotokkal is elmagyarázzuk.</p>

              <p>Az aHang, hogy tovább csökkentse a regisztráció okozta kételyeket, közös, minden kérdést eloszlató Regisztáló Facebook partit szervez június 15-én, vasárnap,  június 18-án este 19 órakor, valamint a nagy vitát követően, június 19-én este 10 órakor a Facebook.com/szabadahang oldalon.</p>
              </div>
            </div>

            <div class="card">
              <div class="card-body">
              </p><strong>A jelöltek</strong></p>
              <p>Június 17-ig mutathatják be az indulni kívánó jelöltek az induláshoz szükséges 2000 ajánlást, amelynek hitelességét az aHang szúrópróbaszerűen ellenőrzi. Karácsony Gergely már összegyűjtötte a megfelelő számú aláírást, a mai nap folyamán a listáját átadja ellenőrzésre az aHangnak. Az elmúlt másfél hétben felbukkanó jelöltek ajánlásait az aHang folyamatosan fogadja, amennyiben a CVB úgy ítéli, hogy az adott jelölt alkalmas a közös értéknyilatkozatban foglaltak képviseletére.</p>
              </div>
            </div>

            <div class="card">
              <div class="card-body">
              </p><strong>A lebonyolítók</strong></p>
              <p>A Dr. Magyar György által elnökölt Civil Választási Bizottság (CVB) egyenlő szavazati joggal bíró tagjai és tagszervezetei jelenleg:</p>
              <ul>
              	<li>Civilek a demokráciáért</li>
              	<li>Mindenki Magyarországa Mozgalom</li>
              	<li>Nyomtass te is!</li>
              	<li>aHang Platform</li>
              	<li>Oktogon Közösség</li>
              	<li>Kálmán Olga kampánystábja</li>
              	<li>Karácsony Gergely kampánystábja</li>
              	<li>Kerpel-Fronius Gábor kampánystábja</li>
              	<li>Szolidaritás Mozgalom</li>
              	<li>DK</li>
              	<li>Jobbik</li>
              	<li>LMP</li>
              	<li>Momentum</li>
              	<li>MSZP</li>
              	<li>Párbeszéd</li>
              	<li>Számoljuk Együtt Mozgalom</li>
              </ul>
              <p>A Budapesti Előválasztás lebonyolítását a jelölteket támogató pártközösségek aktivistákkal és infrastrukturális felajánlásokkal segítik. A civil szervezetek (Civilek a demokráciáért, Nyomtass te is, Számoljuk Együtt Mozgalom, aHang) a szervezésben és a lebonyolításban is biztosítják maguk részéről a pártoktól független, civil szavazóbizottsági- és szavazatszámláló tagokat. Pártoktól függetlennek számít az a civil szervezet, amely nem támogat egyetlen jelöltet sem az előválasztáson, annak kampányában nem vesz részt. Az előválasztás technikai lebonyolítását az aHang végzi.</p>
              </div>
            </div>

            <div class="card">
              <div class="card-body">
              </p><strong>A szavazás menete</strong></p>
              <p>A CVB június 18-án hirdeti ki az indulás feltételeit sikeresen teljesítő jelölteket, másnap, június 19-én rendhagyó, az aHang által szervezett vitára kerül közöttük sor, amelynek keretében szakértők szembesítik őket az egészségügy, a környezetvédelem, a romakérdés és a közlekedés budapesti problémáival és segítségükkel ütköztethetik megoldási javaslataikat az adott területeken.</p>
              <p>Az előválasztás <strong>június 20., csütörtök reggel 10 óra és június 26., szerda este 20 óra között zajlik. A voksokat személyesen az előválasztásra kijelölt sátrakban, vagy előzetes online vagy offline regisztrációt követően az elovalszto.hu felületén adhatják le a budapesti lakosok.</strong></p>
              </div>
            </div>

            <div class="card">
              <div class="card-body">
              </p><strong>Az offline regisztrációnak mi a menete?</strong></p>
              <p>Előválasztásra jogosult minden budapesti személy, aki budapesti lakcímkártyával rendelkezik,  2001. október 1. előtt született, illetve elfogadja és aláírásával az ajánlásgyűjtő íven ellenjegyzi a magyar jogszabályoknak megfelelően, hogy az előválasztást kezdeményező pártok a választás lebonyolítása alatt és érdekében tárolják és kezeljék személyes adatait 2019. június 26-ig bezárólag.</p>
              <p>A szavazósátrakban keletkező szavazatokat június 26-án, szerdán a Civil Választási Bizottság tagszervezetei által delegált közösség közösen számolja meg rögzített menetrendben. A papír alapú szavazatok megszámolásáért a Számoljuk Együtt Mozgalom felel.</p>
              <p>Az eredményt a Civil Választási Bizottság állapítja meg. A Budapesti Előválasztást a legtöbb szavazatot kapó jelölt nyeri (ahogy a főpolgármester-választás során is). Az eredményt a Civil Választási Bizottság minden tagszervezete elfogadja és vállalja, hogy támogatja a megválasztott jelölt indulását. A Budapesti Előválasztás második fordulóját támogató pártok (DK, Jobbik, LMP, Momentum, MSZP, Párbeszéd) vállalják, hogy a Budapesti Előválasztás győztese ellen nem indítanak főpolgármester-jelöltet a 2019. évi önkormányzati választáson.</p>
              <p>A szavazatszámlálás, és eredmény-megállapítás időpontja: <strong>2019. június 26., szerda, 14:00-20:00</strong></p>
              </div>
              </div>

          </div>
        </section>





